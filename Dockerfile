# Extending base Ubuntu 14.04 image with m4, make and lftp for web building
#
# docker build -t dme26/m4-web-builder:v1 .
FROM ubuntu:trusty
MAINTAINER David Eyers <dme@cs.otago.ac.nz>
RUN apt-get update && apt-get install -y \
    lftp \
    m4 \
    make \
    rsync \
    ssh \
    	  && rm -rf /var/lib/apt/lists/*

